/**
 * @author sunaiwen, inspired by http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
 */

(function(){
    var action = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
    exports.raf = function(runner){
        action.call(window, runner);
    };
})();

(function(){
    var action = window.cancelAnimationFrame ||
        window.webkitCancelRequestAnimationFrame ||
        window.mozCancelRequestAnimationFrame ||
        window.oCancelRequestAnimationFrame ||
        window.msCancelRequestAnimationFrame ||
        function(id){
            window.clearTimeout(id);
        };
    exports.cancelRaf = function(){
        return action.apply(window, arguments);
    };
})();
