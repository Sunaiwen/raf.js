(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({"/Users/sunaiwen/projects/raf/raf.js":[function(require,module,exports){
/**
 * @author sunaiwen, inspired by http://www.paulirish.com/2011/requestanimationframe-for-smart-animating/
 */

(function(){
    var action = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.mozRequestAnimationFrame ||
        window.oRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        function (callback) {
            window.setTimeout(callback, 1000 / 60);
        };
    exports.raf = function(runner){
        action.call(window, runner);
    };
})();

(function(){
    var action = window.cancelAnimationFrame ||
        window.webkitCancelRequestAnimationFrame ||
        window.mozCancelRequestAnimationFrame ||
        window.oCancelRequestAnimationFrame ||
        window.msCancelRequestAnimationFrame ||
        function(id){
            window.clearTimeout(id);
        };
    exports.cancelRaf = function(){
        return action.apply(window, arguments);
    };
})();

},{}],"/Users/sunaiwen/projects/raf/test.js":[function(require,module,exports){
var r = require('./raf');

var id;
var rafId;
function run(){
    console.log('I love you.');
    if(id == null) {
        setTimeout(function(){
            r.cancelRaf(rafId)
        }, 1000);
    }
}
function animate (){
    rafId = r.raf(animate);
    run();
}

animate();
},{"./raf":"/Users/sunaiwen/projects/raf/raf.js"}]},{},["/Users/sunaiwen/projects/raf/test.js"]);
